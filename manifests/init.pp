# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include firefox
class firefox (
  Stdlib::Absolutepath  $config,
  Stdlib::Absolutepath  $config_dir,
  String                $config_file_mode,
  Optional[String]      $config_template,
  Optional[String]      $config_epp,
  Hash                  $config_params,
  String                $package_ensure,
  Array[String]         $package_name,
) {

  contain 'firefox::install'
  contain 'firefox::config'

  Class['::firefox::install']
  -> Class['::firefox::config']
}
