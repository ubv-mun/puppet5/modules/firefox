# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include lightdm::config
class firefox::config {

  case $firefox::config_dir {
    '/', '/etc', undef: {}
    default: {
      file { $firefox::config_dir:
        ensure  => directory,
        owner   => 0,
        group   => 0,
        mode    => '0755',
        recurse => false,
      }
    }
  }

  if $firefox::config_epp and $firefox::config_template {
    fail('Cannot supply both config_epp and config_template templates for firefox config file.')
  } elsif $firefox::config_template {
    $config_content = template($firefox::config_template)
  } elsif $firefox::config_epp {
    $config_content = epp($firefox::config_epp)
  } else {
    $config_content = epp('firefox/syspref.js.epp')
  }

  file { $firefox::config:
    ensure  => file,
    owner   => 0,
    group   => 0,
    mode    => $::firefox::config_file_mode,
    content => $config_content,
  }
}
